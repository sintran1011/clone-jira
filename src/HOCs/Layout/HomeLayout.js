import React, { Fragment } from "react";
import { Outlet } from "react-router-dom";

const HomeLayout = () => {
  return (
    <Fragment>
      HomeLayout
      <Outlet />
    </Fragment>
  );
};

export default HomeLayout;
