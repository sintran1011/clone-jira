import React from "react";
import { Outlet } from "react-router-dom";

const SigninLayout = () => {
  return (
    <div className="flex">
      <div
        className="basis-2/3"
        style={{
          backgroundImage: `url(${"https://picsum.photos/1000"})`,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          width: "100%",
          height: "100vh",
        }}
      ></div>
      <div className="h-screen basis-1/3">
        <Outlet />
      </div>
    </div>
  );
};

export default SigninLayout;
