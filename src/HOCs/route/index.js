import { Navigate, Outlet } from "react-router-dom";

export const AuthRoute = ({ condition, navigativePath }) => {
  return condition ? <Outlet /> : <Navigate to={navigativePath} />;
};

export const AccountRoute = ({ condition, navigativePath }) => {
  return condition ? <Navigate to={navigativePath} /> : <Outlet />;
};
