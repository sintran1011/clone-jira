import { actionType } from "./type";
import { createAction } from ".";
import { message } from "antd";
import { register, signin } from "../../services/user";

export const SignUp = (userData) => {
  return async () => {
    try {
      const res = await register(userData);
      if (res.data.statusCode === 200) {
        console.log("register", res.data);
        message.success(res.data.message);
      }
    } catch (err) {
      console.log("register", { ...err });
      message.warning(err.response.data.message);
    }
  };
};

export const SignIn = (userData) => {
  return async (dispatch) => {
    try {
      const res = await signin(userData);
      if (res.data.statusCode === 200) {
        console.log("signin", res.data);
        message.success(res.data.message);
        dispatch(createAction(actionType.SET_LOGIN, res.data));
        localStorage.setItem("jira-token",res.data.content.accessToken)
      }
    } catch (err) {
      console.log("SignIn", { ...err });
      message.warning(err.response.data.message);
    }
  };
};
