import { actionType } from "../actions/type";

const initialState = {
  isLogin: false,
  user: "",
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_LOGIN:
      state.user = payload;
      return { ...state };

    default:
      return state;
  }
};

export default reducer;
