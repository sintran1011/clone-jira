import { baseService } from ".";

class user extends baseService {
  register = (userData) => {
    return this.post(`api/Users/signup`, userData);
  };

  signin = (userData) => {
    return this.post(`api/Users/signin`, userData);
  };
}

const cloneUser = new user();

export const { register, signin } = cloneUser;
