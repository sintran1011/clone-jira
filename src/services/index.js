import axios from "axios";
import { DOMAIN, TOKENCYBERSOFT } from "../util/config";

export class baseService {
  get = (url) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };

  post = (url, userData) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "POST",
      data: userData,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };
}
