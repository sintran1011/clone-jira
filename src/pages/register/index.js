import React from "react";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { Input } from "antd";
import { useFormik } from "formik";
import style from "./style.module.css";
import * as yup from "yup";
import { SignUp } from "../../store/actions/user";
import { useDispatch } from "react-redux";

const schema = yup.object().shape({
  email: yup
    .string()
    .required("Please type your email")
    .email("Email is not valid"),
  passWord: yup.string().required("Please type your password"),
  name: yup.string().required("Please type your name"),
  phoneNumber: yup
    .string()
    .required("Please type your PhoneNumber")
    .matches(/^[0-9]+$/g, "PhoneNumber must be number"),
});

const Register = () => {
  const dispatch = useDispatch();
  const {
    handleChange,
    values,
    touched,
    errors,
    handleBlur,
    isValid,
    setTouched,
  } = useFormik({
    initialValues: {
      email: "",
      passWord: "",
      name: "",
      phoneNumber: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    setTouched({
      email: true,
      passWord: true,
      name: true,
      phoneNumber: true,
    });
    console.log(values);

    if (!isValid) return;

    dispatch(SignUp(values));
  };
  return (
    <div className="h-full bg-gray-200 flex">
      <div className=" p-10 my-auto">
        <h3 className="font-bold text-sky-600 text-[32px] text-center">
          Register new account
        </h3>
        <form onSubmit={handleSubmit}>
          <Input
            className={style["input"]}
            size="large"
            style={{ borderRadius: 8 }}
            placeholder="Enter your email"
            value={values.email}
            onChange={handleChange}
            name="email"
          />
          {touched.email && (
            <span className="text-red-700">{errors.email}</span>
          )}

          <Input.Password
            className={style["input"]}
            size="large"
            style={{ borderRadius: 8 }}
            placeholder="Enter your password"
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={values.passWord}
            onChange={handleChange}
            name="passWord"
          />
          {touched.passWord && (
            <span className="text-red-700">{errors.passWord}</span>
          )}

          <Input
            className={style["input"]}
            size="large"
            style={{ borderRadius: 8 }}
            placeholder="Enter your name"
            value={values.name}
            onChange={handleChange}
            name="name"
          />
          {touched.name && <span className="text-red-700">{errors.name}</span>}

          <Input
            className={style["input"]}
            size="large"
            style={{ borderRadius: 8 }}
            placeholder="Enter your phoneNumber"
            value={values.phoneNumber}
            onChange={handleChange}
            name="phoneNumber"
          />
          {touched.phoneNumber && (
            <span className="text-red-700">{errors.phoneNumber}</span>
          )}

          <div className="flex justify-center">
            <button
              className="my-3 px-7 py-2 rounded text-white text-xl font-bold bg-rose-500 opacity-70 hover:opacity-100"
              type="submit"
            >
              Register
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
