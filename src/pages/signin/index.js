import React from "react";
import { Input } from "antd";
import {
  UserOutlined,
  EyeTwoTone,
  EyeInvisibleOutlined,
} from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { SignIn } from "../../store/actions/user";

const schema = yup.object().shape({
  email: yup
    .string()
    .required("Please type your email")
    .email("Email is not valid"),
  passWord: yup.string().required("Please type your password"),
});

const Login = () => {
  const dispatch = useDispatch();

  const {
    setValues,
    values,
    setTouched,
    handleChange,
    touched,
    errors,
    isValid,
  } = useFormik({
    initialValues: {
      email: "",
      passWord: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const guestLogin = () => {
    const guest = {
      email: "sintran1011@gmail.com",
      passWord: "123456",
    };
    setValues({
      email: guest.email,
      passWord: guest.passWord,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setTouched({
      email: true,
      passWord: true,
    });
    console.log(values);

    if (!isValid) return;

    dispatch(SignIn(values));
  };

  return (
    <div className="h-full bg-gray-200 flex">
      <div className=" p-10 my-auto">
        <h3 className="font-bold text-sky-600 text-[32px] text-center">
          Login to continue
        </h3>
        <form onSubmit={handleSubmit}>
          <Input
            className="my-2"
            size="large"
            style={{ borderRadius: 8 }}
            placeholder="Enter your email"
            prefix={<UserOutlined className="site-form-item-icon" />}
            value={values.email}
            onChange={handleChange}
            name="email"
          />
          {touched.email && (
            <span className="text-red-700">{errors.email}</span>
          )}

          <Input.Password
            className="my-2"
            size="large"
            style={{ borderRadius: 8 }}
            placeholder="Enter your password"
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={values.passWord}
            onChange={handleChange}
            name="passWord"
          />
          {touched.passWord && (
            <span className="text-red-700">{errors.passWord}</span>
          )}

          <div className="flex justify-between">
            <div>
              <input
                className="cursor-pointer"
                type="radio"
                name="rememberMe"
              />
              <span className="text-sm text-black pl-1">Save password</span>
            </div>
            <NavLink to="/Signup">Forgot password?</NavLink>
          </div>

          <div className="flex justify-around">
            <button
              className="my-3 px-7 py-2 rounded text-white text-xl font-bold bg-rose-500 opacity-70 hover:opacity-100"
              type="submit"
            >
              Sign in
            </button>

            <button
              onClick={guestLogin}
              className="my-3 px-7 py-2 rounded text-white text-xl font-bold bg-sky-500 opacity-70 hover:opacity-100"
              type="button"
            >
              Guest account
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
