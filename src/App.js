import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/home";
import Login from "./pages/signin";
import HomeLayout from "./HOCs/Layout/HomeLayout";
import SigninLayout from "./HOCs/Layout/SigninLayout";
import Register from "./pages/register";
import { AccountRoute, AuthRoute } from "./HOCs/route";

function App() {
  const token = localStorage.getItem("jira-token");
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomeLayout />}>
          <Route
            element={
              <AuthRoute condition={token} navigativePath="/User/Login" />
            }
          >
            <Route index element={<Home />} />
          </Route>
        </Route>
      </Routes>
      <Routes>
        <Route path="User" element={<SigninLayout />}>
          <Route
            element={<AccountRoute condition={token} navigativePath="/" />}
          >
            <Route path="Login" element={<Login />} />
          </Route>

          <Route
            element={<AccountRoute condition={token} navigativePath="/" />}
          >
            <Route path="Register" element={<Register />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
